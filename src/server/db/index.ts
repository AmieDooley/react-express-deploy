import * as mysql from 'mysql';
import config from '../config';

import User from './main'

export const con = mysql.createConnection(config.mysql);

con.connect(err => {
    if(err) console.log(err);
});

export default {
    User
}