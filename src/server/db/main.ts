import {con} from './index';

export const all = async () => {
    return new Promise((resolve, reject) => {
        con.query('SELECT * from users', (err, results) => {
            if (err) {
                return reject(err);
            } else {
                resolve(results);
            }
        });
    });
}

export default {
    all
}