import * as express from 'express';
import { resourceLimits } from 'node:worker_threads';
import db from './db'
const router = express.Router();

router.get('/api/hello', (req, res, next) => {
    res.json('World');
});

router.get('/api/users', async(req, res) => {
    try{
        let users = await db.User.all();
        res.json(users);
    } catch (err) {
        console.log(err);
        res.sendStatus(err);
    }
})

export default router;