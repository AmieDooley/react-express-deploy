import * as React from 'react';

const App = (props: AppProps) => {
	const [greeting, setGreeting] = React.useState<string>('');
	const [users, setUsers] = React.useState<Array<any>>([]);

	React.useEffect(() => {
		(async () => {
			try {
				const res = await fetch('/api/users');
				const greeting = await res.json();
				console.log('users', greeting);
				setUsers(greeting);
			} catch (error) {
				console.log(error);
			}
		})();
	}, []);

	return (
		<div className="min-vh-100 d-flex justify-content-center align-items-center">
			<h1 className="display-1">Hello {users}!</h1>
		</div>
	);
};

interface AppProps {}

export default App;
